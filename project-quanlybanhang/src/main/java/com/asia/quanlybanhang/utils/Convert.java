package com.asia.quanlybanhang.utils;

import com.asia.quanlybanhang.entity.Role;
import com.asia.quanlybanhang.entity.User;
import com.asia.quanlybanhang.repository.UserRepository;
import com.asia.quanlybanhang.security.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class Convert {

    @Autowired
    private UserRepository userRepository;

    public static CustomUserDetails convertToCustomUserDetails(User user){
        CustomUserDetails customUserDetails = new CustomUserDetails();
        customUserDetails.setId(user.getId());
        customUserDetails.setUsername(user.getUsername());
        customUserDetails.setPassword(user.getPassword());
        customUserDetails.setEmail(user.getEmail());
        customUserDetails.setPhoneNumber(user.getPhoneNumber());

        List<Role> roles = user.getRoles();
        List<GrantedAuthority> authorities = new ArrayList<>();

        for (Role role : roles){
            authorities.add(new SimpleGrantedAuthority("ROLE_"+role.getName()));
        }

        customUserDetails.setAuthorities(authorities);

        return customUserDetails;
    }
}
