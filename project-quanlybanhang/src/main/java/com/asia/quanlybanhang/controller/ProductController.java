package com.asia.quanlybanhang.controller;

import com.asia.quanlybanhang.entity.Products;
import com.asia.quanlybanhang.service.ProductService;
import com.asia.quanlybanhang.service.impl.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/products")
@SessionAttributes("keyword")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public String productPage(ModelMap modelMap, HttpSession httpSession){

        String userName = (String) httpSession.getAttribute("username");

        modelMap.addAttribute("userName", userName);

        return findPaginated(1, modelMap);
    }

    @GetMapping("add-product")
    public String addProduct(ModelMap modelMap){

        modelMap.addAttribute("products", new Products());
        return "addproduct";
    }

    @PostMapping("/save-product")
    public String saveProduct(Products products){
        productService.saveProduct(products);
        return "redirect:/products";
    }

    @GetMapping("/edit-product")
    public String editProduct(@RequestParam Long id,
                              ModelMap modelMap){

        Optional<Products> productEdit = productService.findProductById(id);

        productEdit.ifPresent(products -> modelMap.addAttribute("products", products));

        return "editproduct";

    }

    @GetMapping("/delete-product")
    public String deleteProduct(@RequestParam Long id){

        productService.deleteProducts(id);
        return "redirect:/products";
    }

    @GetMapping("/page/{pageNo}")
    public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
                                ModelMap modelMap){

        int pageSize = 2;

        Page<Products> page = productService.findProductsPaginated(pageNo, pageSize);

        List<Products> listProducts = page.getContent();

        modelMap.addAttribute("currentPage", pageNo);
        modelMap.addAttribute("totalPages", page.getTotalPages());
        modelMap.addAttribute("totalItems", page.getTotalElements());
        modelMap.addAttribute("listProducts", listProducts);

        return "products";
    }

    @GetMapping("/search-product/{pageNo}")
    public String searchPoductPagination(@PathVariable int pageNo,
                                         @RequestParam String keyword,
                                         ModelMap modelMap){

        int pageSize = 2;

        Page<Products> page = productService.searchProductsPaginated(keyword, pageNo, pageSize);
        List<Products> listProducts = page.getContent();

        modelMap.addAttribute("currentPage", pageNo);
        modelMap.addAttribute("totalPages", page.getTotalPages());
        modelMap.addAttribute("totalItems", page.getTotalElements());
        modelMap.addAttribute("listProducts", listProducts);
        modelMap.addAttribute("keyword", keyword);
        return "searchproduct.html";
    }
}
