package com.asia.quanlybanhang.controller;

import com.asia.quanlybanhang.entity.User;
import com.asia.quanlybanhang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping
    public String registerPage(){
        return "register";
    }

    @PostMapping
    public String register(@RequestParam String userName,
                           @RequestParam String password,
                           @RequestParam String retypepassword,
                           ModelMap modelMap){

        boolean checkUserName = validate(userName);

        if (checkUserName){
            if (password.equals(retypepassword)){
                User user = new User();
                user.setUsername(userName);
                user.setPassword(passwordEncoder.encode(password)); // ma hoa mat khau nguoi dung
                boolean checkInsertUser = userService.insertUser(user);

                if (checkInsertUser){
                    modelMap.addAttribute("checklogin", "Đăng ký thành công");
                }else{
                    modelMap.addAttribute("checkLogin", "Đăng ký thất bại");
                }
            }else {
                modelMap.addAttribute("checkLogin", "Mật khẩu không trùng khớp");
            }
        }else{
            modelMap.addAttribute("checkLogin", "Username không đúng định dạng");
        }

        return "register";
    }

    public static final Pattern VALID_USERNAME_ADDRESS_REGEX =
            Pattern.compile("^[a-z0-9_-]{3,15}$", Pattern.CASE_INSENSITIVE);

    public static boolean validate(String userName){
        Matcher matcher = VALID_USERNAME_ADDRESS_REGEX.matcher(userName);
        return matcher.find();
    }
}
