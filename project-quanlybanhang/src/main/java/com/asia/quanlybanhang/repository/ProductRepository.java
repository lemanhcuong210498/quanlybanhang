package com.asia.quanlybanhang.repository;

import com.asia.quanlybanhang.entity.Products;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Products, Long> {

    @Query(value = "SELECT * FROM products p WHERE p.name LIKE %?1% OR p.description LIKE %?1%", nativeQuery = true)
    public List<Products> searchProductByKeyWord(String keyword);

    Page<Products> findProductsByNameContaining(String keyword, Pageable pageable);

}
