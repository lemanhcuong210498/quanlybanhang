package com.asia.quanlybanhang.service.impl;

import com.asia.quanlybanhang.entity.User;
import com.asia.quanlybanhang.repository.UserRepository;
import com.asia.quanlybanhang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean checkLogin(String userName, String password) {

        try{
            User user = userRepository.findUserByUsernameAndPassword(userName, password);
            if (user != null)
                return true;
            return false;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean insertUser(User user) {

        User user1 = userRepository.save(user);

        //kiem tra user da duoc luu vao database hay chua, co null ko
        if (user1.getId() > 0){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public User findUserByUsernameAndPassword(String username, String pass) {
        User user = userRepository.findUserByUsernameAndPassword(username, pass);
        return user;
    }

    @Override
    public String findRoleById(Long id) {
        return userRepository.findRoleById(id);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User loadUserById(Long id) {
        return userRepository.findUserById(id);
    }
}
