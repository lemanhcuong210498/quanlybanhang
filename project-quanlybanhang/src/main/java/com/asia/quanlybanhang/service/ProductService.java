package com.asia.quanlybanhang.service;

import com.asia.quanlybanhang.entity.Products;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    List<Products> getAllProducts();

    void saveProduct(Products products);

    void deleteProducts(Long id);

    Optional<Products> findProductById(Long id);

    List<Products> searchProducts(String keyword);

    Page<Products> findProductsPaginated(int pageNo, int pageSize);

    Page<Products> searchProductsPaginated(String keyword, int pageNo, int pageSize);

}
