package com.asia.quanlybanhang.service;

import com.asia.quanlybanhang.entity.User;

public interface UserService {

    boolean checkLogin(String userName, String password);

    boolean insertUser(User user);

    User findUserByUsernameAndPassword(String username, String pass);

    String findRoleById(Long id);

    User findByUsername(String username);

    User loadUserById(Long id);
}
