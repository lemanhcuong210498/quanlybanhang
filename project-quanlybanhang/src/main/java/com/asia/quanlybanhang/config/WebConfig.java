package com.asia.quanlybanhang.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler ("/ images/**")
//                .addResourceLocations ("/ images /");
        registry.addResourceHandler("/**",
                "/css/**",
                "/js/**",
                "/assets/**",
                "/images/**",
                "/login/**")
                    .addResourceLocations(
                            "classpath:/static/css/",
                            "classpath:/static/js/",
                            "classpath:/static/assets/",
                            "classpath:/static/images/",
                            "classpath:/static/login/");

    }
}

